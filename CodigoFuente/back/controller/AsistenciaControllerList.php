<?php
include_once realpath('../facade/Otras_actividadesFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);
$id_producto = strip_tags($dataObject->id_producto);

$list = Otras_actividadesFacade::getListadoAsistenciaByProducto($id_producto);

echo json_encode(["data" => $list]);

