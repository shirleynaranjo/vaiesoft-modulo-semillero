<?php
include_once realpath('../facade/Otras_actividadesFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$idSemillero = strip_tags($dataObject->idSemillero);
$idProducto = strip_tags($dataObject->idProducto);
$data = array(
    "id_semillero" => $idSemillero,
    "id_producto" => $idProducto,
);
$rpta = Otras_actividadesFacade::marcarAsistencia($data);

try
{
    if ($rpta > 0)
    {
        http_response_code(200);
        echo "{\"mensaje\":\"Se ha registrado exitosamente\"}";
    }
}
catch(Exception $e)
{
    http_response_code(500);
    echo "{\"mensaje\":\"Error al registrar \"}";
}

