<?php

include_once realpath('../facade/ProyectosFacade.php');
include_once realpath('../facade/Proy_lineas_investFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$rpta = "";



    $id = strip_tags($dataObject->id);
    


    $rpta = ProyectosFacade::updateTerminarTodo($id);


try {
    if ($rpta >= 0) {
        http_response_code(200);
        echo "{\"mensaje\":\"Se ha registrado exitosamente\"}";
    }
} catch (Exception $e) {
    http_response_code(500);
    echo "{\"mensaje\":\"Error al registrar el proyecto\"}";
}
