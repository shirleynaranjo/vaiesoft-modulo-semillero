<?php
include_once realpath('../facade/DocenteFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$codigo = strip_tags($dataObject->codigo);
$rta = DocenteFacade::existe($codigo);
http_response_code(200);
echo json_encode(["existe" => $rta]);

