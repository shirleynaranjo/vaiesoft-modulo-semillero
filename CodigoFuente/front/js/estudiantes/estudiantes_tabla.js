$(document).ready(function() {
    iniciarTablaEstudiantes();
    obtenerDatos_Estudiantes();
    cargarSelectPlanesE();
    cargarSelectTipodoc();
//    cargarSelectInspector();
//    ocultarModalOrdenes();

    $("#btnEstuReg").show();
    $("#btnEstuAct").hide();
});

//----------------------------------TABLA----------------------------------

/**
 * @method iniciarTabla
 * Metodo para instanciar la DataTable
 */
function iniciarTablaEstudiantes() {

    //tabla de alumnos
    $("#listadoTablaEstudiantes2").DataTable({
        responsive: true,
        ordering: true,
        paging: true,
        searching: true,
        info: true,
        lengthChange: false,
        language: {
            emptyTable: "Sin Ordenes...",
            search: "Buscar:",
            info: "_START_ de _MAX_ registros", //_END_ muestra donde acaba _TOTAL_ muestra el total
            infoEmpty: "Ningun registro 0 de 0",
            infoFiltered: "(filtro de _MAX_ registros en total)",
            paginate: {
                first: "Primero",
                previous: "Anterior",
                next: "Siguiente",
                last: "Ultimo"
            }
        },
        columns: [
            {
                data: "nombre",
                className: "text-center",
                orderable: true,
            },
            {
                data: "codigo",
                className: "text-center",
                orderable: true,
            },
            {
                data: "correo",
                className: "text-center",
                orderable: true,
            },

            {
                data: "programa_academico",
                className: "text-center",
                orderable: true,
            },
            {
                data: "telefono",
                className: "text-center",
                orderable: true,
            },
            
            {
                orderable: false,
                defaultContent: [
                    "<div class='text-center'>",
                    "<a class='personalizado actualizar' title='Gestionar'><i class='fa fa-edit'></i>&nbsp; &nbsp;  &nbsp;</a>",
                    "<a class='personalizado eliminar' title='eliminar'><i class='fa fa-trash'></i></a>",
                    "</div>",
                ].join(""),
            },
        ],
        rowCallback: function(row, data, index) {
            var id_order = data.id
            var id_perso = data.persona_id_id
          
            $(".actualizar", row).click(function() {
       
                gestionarItemEstu( data);
            });
            $(".eliminar", row).click(function() {
                DeleteEstudiante(id_order,id_perso);
            });
        },
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
                extend: "copy",
                className: "btn btn-primary glyphicon glyphicon-duplicate"
            },
            {
                extend: "csv",
                title: "listadoAlumnos",
                className: "btn btn-primary glyphicon glyphicon-save-file"
            },
            {
                extend: "excel",
                title: "listadoAlumnos",
                className: "btn btn-primary glyphicon glyphicon-list-alt"
            },
            {
                extend: "pdf",
                title: "listadoAlumnos",
                className: "btn btn-primary glyphicon glyphicon-file"
            },
            {
                extend: "print",
                className: "btn btn-primary glyphicon glyphicon-print",
                customize: function(win) {
                    $(win.document.body).addClass("white-bg");
                    $(win.document.body).css("font-size", "10px");
                    $(win.document.body)
                        .find("table")
                        .addClass("compact")
                        .css("font-size", "inherit");
                },
            },
        ],
    });

}

function cerrarModalEstudiantes () {
       $('#myModalEstudiantesE').modal('hide');
}
//----------------------------------CRUD----------------------------------
/**
 * @method obtenerDatos
 * Método que se encarga de consumir el servicio que devuelve la data para la tabla de alumnos.
 */

function obtenerDatos_Estudiantes() {
   
  let  semillero_id = Utilitario.getLocal("id_semillero")
    let order = {
    semillero_id: semillero_id,
  };
    
    Utilitario.agregarMascara();
    fetch("../../back/controller/EstudianteController_list_Sem.php", {

    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",

      Plataform: "web",
    },
    body: JSON.stringify(order),
  })
        .then(function(response) {
            if (response.ok) {
                return response.json();
            }
            throw response;
        })
        .then(function(data) {
            listadoEstudiantesS(data.Estud);
        })
        .catch(function(promise) {
            if (promise.json) {
                promise.json().then(function(response) {
                    let status = promise.status,
                        mensaje = response ? response.mensaje : "";
                    if (status === 401 && mensaje) {
                        Mensaje.mostrarMsjWarning("Advertencia", mensaje, function() {
                            Utilitario.cerrarSesion();
                        });
                    } else if (mensaje) {
                        Mensaje.mostrarMsjError("Error", mensaje);
                    }
                });
            } else {
                Mensaje.mostrarMsjError(
                    "Error",
                    "Ocurrió un error inesperado. Intentelo nuevamente por favor."
                );
            }
        })
        .finally(function() {
            Utilitario.quitarMascara();
        });
};

//----------------------------------HELPERS----------------------------------

/**
 * @method listadoEspecialOrdenes
 * Método que se encarga de listar los alumnos a la tabla.
 *
 * @param {Object} orders Arreglo con los datos de las ordenes.
 */


function listadoEstudiantesS(Estud) {

    let tabla = $("#listadoTablaEstudiantes2").DataTable();
    tabla.data().clear();
    tabla.rows.add(Estud).draw();
}

/**
 * @method selectInspectores
 * Método que se encarga de listar los alumnos a la tabla.
 *
 * @param {Object} orders Arreglo con los datos de las ordenes.
 */
function mostrarModalEstudiantesE() {
//    limpiarcampos();
   $('#myModalEstudiantesE').modal({show: true});
    $("#btnOrderReg").show();
    $("#btnOrderAct").hide();
}

/**
 * @method ocultarModalOrdenes
 * Método que se encarga de cerrar el modal para registro o actualizacion
 */
function cerrarModalEstuR () {
       $('#myModalEstudiantesE').modal('hide');
}

/**
 * @method limpiarcampos
 * Método que se encarga de limpiar los campos del modal para registro o actualizacion
 */
function limpiarcampos() {
    $('#idOrder').val('');
    $('#estu_correo').val('');
    $('#estu_telefono').val('');
    $('#estu_nombre').val('');
    $('#estu_semestre').val('');
    $('#estu_programa_academico').val('');
    $('#estu_tipo_docuemnto_id').val('');
    $('#estu_num_documento').val('');
   
    
    
}
function gestionarItemEstu(data) {
    
    
    alert(data.persona_id_id);
    $('#estu_id').val(data.id);
    $('#persona_id').val(data.persona_id_id);
    $('#estu_correo').val(data.correo);
    $('#estu_telefono').val(data.telefono);
    $('#estu_nombre').val(data.nombre);
    $('#estu_semestre').val(data.semestre);
    $('#estu_programa_academico').val(data.programa_academico);
    $('#estu_tipo_docuemnto_id').val(data.tipo_docuemnto_id_id);
    $('#estu_num_documento').val(data.num_documento);
    $('#estu_codigo').val(data.codigo);
   
     $('#myModalEstudiantesE').modal({show: true});
    $("#btnEstuReg").hide();
    $("#btnEstuAct").show();
    
}

function cargarSelectPlanesE() {
  fetch("../../back/controller/Plan_estudiosController_List_All.php", {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      //                Authorization: JSON.parse(Utilitario2.getLocal("user")).token,
      Plataform: "web",
    },
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      construirPlanesE(data.plan);
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario2.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjError(
          "Error",
          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
        );
      }
    });
}

/**
 * @method construirSelectNacionalidad
 * construye y agrega los tipos al contenedor
 */
function construirPlanesE(plan) {
    
  $("#estu_programa_academico").empty();
  let input = $("#estu_programa_academico");
  let opcion = new Option("SELECCIONE", "");
  $(opcion).html("SELECCIONE");
  input.append(opcion);
  for (let index = 0; index < plan.length; index++) {
    let planes = plan[index],
      opcion = new Option(planes.descripcion, planes.id);
    $(opcion).html(planes.descripcion);
    input.append(opcion);
  }
}


function cargarSelectTipodoc() {
  fetch("../../back/controller/Tipo_docuemntoControllerList.php", {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      //                Authorization: JSON.parse(Utilitario2.getLocal("user")).token,
      Plataform: "web",
    },
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      construirTpDE(data.tipo_doc);
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario2.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
//        Mensaje.mostrarMsjError(
//          "Error",
//          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
//        );
      }
    });
}

/**
 * @method construirSelectNacionalidad
 * construye y agrega los tipos al contenedor
 */
function construirTpDE(tipo_doc) {
    
  $("#estu_tipo_docuemnto_id").empty();
  let input = $("#estu_tipo_docuemnto_id");
  let opcion = new Option("SELECCIONE", "");
  $(opcion).html("SELECCIONE");
  input.append(opcion);
  for (let index = 0; index < tipo_doc.length; index++) {
    let tipo_docS = tipo_doc[index],
      opcion = new Option(tipo_docS.descripcion, tipo_docS.id);
    $(opcion).html(tipo_docS.descripcion);
    input.append(opcion);
  }
}

async function registrarEstuE() {
    semille = Utilitario.getLocal("id_semillero");
    codigo= $('#estu_codigo').val();
    let ordenes = {
        semi_id: semille,
        estu_correo: $('#estu_correo').val(),
        estu_telefono: $('#estu_telefono').val(),
        estu_nombre: $('#estu_nombre').val(),
        estu_codigo: codigo,
        estu_semestre: $('#estu_semestre').val(),
        estu_programa_academico: $("#estu_programa_academico option:selected").html(),
        estu_tipo_docuemnto_id: $('#estu_tipo_docuemnto_id').val(),
        estu_num_documento: $('#estu_num_documento').val()
    };

  let existe = await existeEstudiante(codigo);
  console.log(existe);
  if (existe) {
      Mensaje.mostrarMsjWarning("Informacion", "Ya existe un usuario registado con el codigo " + codigo);
      return null;
  }

    Utilitario.agregarMascara();
    fetch("../../back/controller/SemilleroController_insertEstu.php", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                // Authorization: JSON.parse(Utilitario.getLocal("user")).token,
                Plataform: "web",
            },
            body: JSON.stringify(ordenes),
        })
        .then(function(response) {
            if (response.ok) {
                return response.json();
            }
            throw response;
        })
        .then(function(data) {

            Mensaje.mostrarMsjExito("Registro Exitoso", data.mensaje);
            obtenerDatos_Estudiantes();
      
        })
        .catch(function(promise) {
            if (promise.json) {
                promise.json().then(function(response) {
                    let status = promise.status,
                        mensaje = response ? response.mensaje : "";
                    if (status === 401 && mensaje) {
                        Mensaje.mostrarMsjWarning("Advertencia", mensaje, function() {
                            Utilitario.cerrarSesion();

                        });
                    } else if (mensaje) {
                        Mensaje.mostrarMsjError("Error", mensaje);
                    }
                });
            } else {
                Mensaje.mostrarMsjError(
                    "Error",
                    "Ocurrió un error inesperado. Intentelo nuevamente por favor."
                );
            }
        })
        .finally(function() {
            Utilitario.quitarMascara();
        });

}

  async function existeEstudiante(codigo) {
      try {
        let data = await fetch("../../back/controller/EstudianteExisteController.php", {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",        
              Plataform: "web",
            },        
            body: JSON.stringify({codigo}),
          })
          data = await data.json();
          return data.existe.length;
      } catch (error) {
          
      }
  }



function UpdateEstuE() {




    let update_estu = {
        estu_id: $('#estu_id').val(),
        persona_id_id: $('#persona_id').val(),
        estu_nombre: $('#estu_nombre').val(),
        estu_telefono: $('#estu_telefono').val(),
        estu_correo: $('#estu_correo').val(),
        estu_codigo: $('#estu_codigo').val(),
        estu_semestre: $('#estu_semestre').val(),
        estu_programa_academico: $("#estu_programa_academico option:selected").html(),
        estu_tipo_docuemnto_id: $('#estu_tipo_docuemnto_id').val(),
        estu_num_documento: $('#estu_num_documento').val()
    };

    Utilitario.agregarMascara();
    fetch("../../back/controller/SemilleroController_UpdateEstu.php", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                // Authorization: JSON.parse(Utilitario.getLocal("user")).token,
                Plataform: "web",
            },
            body: JSON.stringify(update_estu),
        })
        .then(function(response) {
            if (response.ok) {
                return response.json();
            }
            throw response;
        })
        .then(function(data) {
             obtenerDatos_Estudiantes();
            cerrarModalEstuR();
            Mensaje.mostrarMsjExito("Registro Exitoso", data.mensaje);
        })
        .catch(function(promise) {
            if (promise.json) {
                promise.json().then(function(response) {
                    let status = promise.status,
                        mensaje = response ? response.mensaje : "";
                    if (status === 401 && mensaje) {
                        Mensaje.mostrarMsjWarning("Advertencia", mensaje, function() {
                            Utilitario.cerrarSesion();
                        });
                    } else if (mensaje) {
                        Mensaje.mostrarMsjError("Error", mensaje);
                    }
                });
            } else {
                Mensaje.mostrarMsjError(
                    "Error",
                    "Ocurrió un error inesperado. Intentelo nuevamente por favor."
                );
            }
        })
        .finally(function() {
            Utilitario.quitarMascara();
        });

}

async function gestionarEstu(id, data, index) {
    let data_plan = await getDataPlan();
    if (data_plan.plan) {
        construirSelect('estu_programa_academico', data_plan.plan)
        $("#estu_programa_academico option:contains(" + data.programa_academico + ")").attr('selected', 'selected');
    }
    let data_tipo_doc = await getDataTipoDoc();
    if (data_tipo_doc.tipo_doc) {
        construirSelect('estu_tipo_docuemnto_id', data_tipo_doc.tipo_doc)
        $('#estu_tipo_docuemnto_id').val(data.tipo_docuemnto_id_id);
    }

    $('#estu_id').val(id);
    $('#persona_id').val(persona_id_id);
    $('#estu_index').val(index);
    $('#estu_persona_id_id').val(data.persona_id_id);
    $('#estu_nombre').val(data.nombre);

    $('#estu_num_documento').val(data.num_documento);
    $('#estu_codigo').val(data.codigo);
    $('#estu_semestre').val(data.semestre);

    $('#estu_telefono').val(data.telefono);
    $('#estu_correo').val(data.correo);

    Utilitario.validForm(['estu_correo', 'estu_telefono', 'estu_nombre',
        'estu_codigo', 'estu_semestre', 'estu_programa_academico',
        'estu_tipo_docuemnto_id', 'estu_num_documento'
    ]);

    $("#btnEstuAct").show();
    $("#btnEstuReg").hide();
    $('#myModalEstudiantes').modal({ show: true });
}



function DeleteEstudiante(id_order,id_perso) {
    Mensaje.mostrarMsjConfirmacion(
        'Eliminar Estudiante',
        'Este proceso es irreversible , ¿esta seguro que desea eliminar la Orden?',
        function() {
            eliminarEstu(id_order,id_perso);
        }
    );
}



function eliminarEstu(id_order,id_perso) {
   
   id=id_order
    persona_id=id_perso
  let data = {
    id: id,
    persona_id_id:persona_id,
  };
  Utilitario.agregarMascara();
  fetch("../../back/controller/EstudianteController_DeleteEstu.php", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",

      Plataform: "web",
    },

    body: JSON.stringify(data),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      Mensaje.mostrarMsjExito("Borrado Exitoso", data.mensaje);

      obtenerDatos_Estudiantes();
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjError(
          "Error",
          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
        );
      }
    })
    .finally(function () {
      Utilitario.quitarMascara();
    });
}




