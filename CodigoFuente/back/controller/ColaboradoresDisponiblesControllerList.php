<?php
include_once realpath('../facade/ColaboradorFacade.php');

$list = ColaboradorFacade::listarColaboradoresDisponibles();
http_response_code(200);
echo json_encode(["colaboradores" => $list]);

