<?php

include_once realpath('../facade/EstudianteFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$id = strip_tags($dataObject->proyecto_id);

$list = EstudianteFacade::listAll_Semillero_select($id);
$rta = "";
foreach ($list as $obj => $Estudiante) {
    $rta .= "{
	    \"id_estudiante\":\"{$Estudiante->getid()}\",
	    \"nombre\":\"{$Estudiante->getpersona_id()->getNombre()}\",
	    \"codigo\":\"{$Estudiante->getcodigo()}\",
	    \"id_persona\":\"{$Estudiante->getpersona_id()->getid()}\"
	       },";
}

if ($rta != "") {
    $rta = substr($rta, 0, -1);
    http_response_code(200);
    echo "{\"estudiante\":[{$rta}]}";
} else {
    echo "{\"estudiante\":[]}";
}
