<?php
include_once realpath('../facade/Otras_actividadesFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$producto = strip_tags($dataObject->producto);
$descripcion = strip_tags($dataObject->descripcion);
$responsable = strip_tags($dataObject->responsable);
$fecha = strip_tags($dataObject->fecha);
$calificacion = strip_tags($dataObject->calificacion);
$data = array(
    "producto" => $producto,
    "descripcion" => $descripcion,
    "responsable" => $responsable,
    "fecha" => $fecha,
    "calificacion" => $calificacion
);

$rpta = Otras_actividadesFacade::insertGestion($data);
try
{
    if ($rpta > 0)
    {
        http_response_code(200);
        echo "{\"mensaje\":\"Se ha registrado exitosamente\"}";
    }
}
catch(Exception $e)
{
    http_response_code(500);
    echo "{\"mensaje\":\"Error al registrar \"}";
}

