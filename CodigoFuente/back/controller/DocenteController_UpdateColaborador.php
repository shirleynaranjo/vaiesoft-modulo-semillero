<?php
include_once realpath('../facade/DocenteFacade.php');
include_once realpath('../facade/PersonaFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$nombre = strip_tags($dataObject->nombre);
$telefono = strip_tags($dataObject->telefono);
$correo = strip_tags($dataObject->correo);
$codigo = strip_tags($dataObject->codigo);
$id_docente = strip_tags($dataObject->id_docente);
$id_persona = strip_tags($dataObject->id_persona);

$rptaP = PersonaFacade::updateColaborador($nombre,  $telefono,  $correo, $id_persona);

$rpta99 = DocenteFacade::updateColaborador($codigo, $id_docente);
http_response_code(200);
echo "{\"mensaje\":\"Se ha registrado exitosamente\"}";

