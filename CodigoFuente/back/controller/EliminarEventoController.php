<?php
include_once realpath('../facade/Otras_actividadesFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$id = strip_tags($dataObject->id);
Otras_actividadesFacade::eliminarEvento($id);
http_response_code(200);
echo "{\"mensaje\":\"Se ha eliminado exitosamente\"}";

