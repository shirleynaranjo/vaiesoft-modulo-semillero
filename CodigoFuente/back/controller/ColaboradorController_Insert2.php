<?php
include_once realpath('../facade/ColaboradorFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$id_persona = strip_tags($dataObject-> id_persona);
$tp_colaborador = strip_tags($dataObject->tipo);
$id_proyecto = strip_tags($dataObject->id_proyecto);
    try {
        $rpta = ColaboradorFacade::insertCooinvestigador($id_persona, $tp_colaborador, $id_proyecto);
        if ($rpta > 0) {
            http_response_code(200);
            echo "{\"mensaje\":\"Se ha registrado exitosamente\"}";
        }
    } catch (Exception $e) {
        http_response_code(500);
        echo "{\"mensaje\":\"Error al registrar\"}";
    }