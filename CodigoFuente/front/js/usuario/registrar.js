function registrarUsuario() {
  let data = {
    nombreD: document.querySelector("#nombre").value,
    telefonoD: document.querySelector("#telefono").value,
    correoD: document.querySelector("#correo").value,
    perfil: document.querySelector("#perfil").value,
  };
  fetch("../../back/controller/UsuarioController_InserU.php", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Plataform: "web",
    },
    body: JSON.stringify(data),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      Mensaje.mostrarMsjExito(
        "Registrado",
        "Se ha registrado el nuevo usuario"
      );
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjExito(
          "Registrado",
          "Se ha registrado el nuevo usuario"
        );
      }
    });
}
