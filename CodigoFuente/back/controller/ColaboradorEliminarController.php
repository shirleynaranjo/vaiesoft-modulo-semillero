<?php
include_once realpath('../facade/DocenteFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$id = strip_tags($dataObject->id);
DocenteFacade::eliminarColaborador($id);
http_response_code(200);
echo "{\"mensaje\":\"Se ha eliminado exitosamente\"}";

