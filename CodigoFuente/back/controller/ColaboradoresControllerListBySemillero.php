<?php
include_once realpath('../facade/DocenteFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$idSemillero = strip_tags($dataObject->id_semillero);

$list = DocenteFacade::listColaboradoresBySemillero($idSemillero);
http_response_code(200);
echo json_encode(["colaboradores" => $list]);

