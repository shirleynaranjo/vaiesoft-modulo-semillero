<?php
include_once realpath('../facade/DocenteFacade.php');
include_once realpath('../facade/PersonaFacade.php');

$JSONData = file_get_contents("php://input");
$dataObject = json_decode($JSONData);

$nombre = strip_tags($dataObject->nombre);
$telefono = strip_tags($dataObject->telefono);
$correo = strip_tags($dataObject->correo);
$codigo = strip_tags($dataObject->codigo);
$id_semillero = strip_tags($dataObject->id_semillero);
$Perfiles_id = "4";

$password = " ";
$perfiles = new Perfiles();
$perfiles->setId($Perfiles_id);
$rptaP = PersonaFacade::insert($nombre, $telefono, $correo, $perfiles, $password);

/*registrar docente*/
$Persona_id = $rptaP;
$persona = new Persona();
$persona->setId($Persona_id);
$Tipo_Docente = '1';
$Tipo_vinculacion = '1';

$rpta99 = DocenteFacade::insert_colaborador($Persona_id, $Tipo_Docente, $Tipo_vinculacion, $id_semillero, $codigo);
http_response_code(200);
echo "{\"mensaje\":\"Se ha registrado exitosamente\"}";

