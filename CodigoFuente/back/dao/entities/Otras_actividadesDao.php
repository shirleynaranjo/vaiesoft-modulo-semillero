<?php
/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
 */

//    Soy la sonrisa burlona y vengativa de Jack  \\

include_once realpath('../dao/interfaz/IOtras_actividadesDao.php');
include_once realpath('../dto/Otras_actividades.php');
include_once realpath('../dto/Semillero.php');

class Otras_actividadesDao implements IOtras_actividadesDao{

private $cn;

    /**
     * Inicializa una única conexión a la base de datos, que se usará para cada consulta.
     */
    function __construct($conexion) {
            $this->cn =$conexion;
    }

    /**
     * Guarda un objeto Otras_actividades en la base de datos.
     * @param otras_actividades objeto a guardar
     * @return  Valor asignado a la llave primaria 
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
  public function insert($otras_actividades){
      $id=$otras_actividades->getId();
$nombre_proyecto=$otras_actividades->getNombre_proyecto();
$nombre_actividad=$otras_actividades->getNombre_actividad();
$modalidad_participacion=$otras_actividades->getModalidad_participacion();
$responsable=$otras_actividades->getResponsable();
$fecha_realizacion=$otras_actividades->getFecha_realizacion();
$producto=$otras_actividades->getProducto();
$semillero_id=$otras_actividades->getSemillero_id()->getId();

      try {
          $sql= "INSERT INTO `otras_actividades`( `id`, `nombre_proyecto`, `nombre_actividad`, `modalidad_participacion`, `responsable`, `fecha_realizacion`, `producto`, `semillero_id`)"
          ."VALUES ('$id','$nombre_proyecto','$nombre_actividad','$modalidad_participacion','$responsable','$fecha_realizacion','$producto','$semillero_id')";
          return $this->insertarConsulta($sql);
      } catch (SQLException $e) {
          throw new Exception('Primary key is null');
      }
  }
  
  public function insert2($nombre_proyecto, $nombre_actividad, $modalidad_participacion, $responsable, $fecha_realizacion, $producto, $Semillero_id,$linea_investigacion_id,$proyectos_id,$plan_accion_id){
   


      try {
          $sql= "INSERT INTO `otras_actividades`(  `nombre_proyecto`, `nombre_actividad`, `modalidad_participacion`, `responsable`, `fecha_realizacion`, `producto`, `semillero_id`, `plan_accion_id` )"
          ."VALUES ('$nombre_proyecto', '$nombre_actividad', '$modalidad_participacion', '$responsable', '$fecha_realizacion', '$producto', '$Semillero_id','$plan_accion_id')";

          return $this->insertarConsulta($sql);
      } catch (SQLException $e) {
          throw new Exception('Primary key is null');
      }
  }
  public function insertGestion($data){
    try {
        $producto = $data["producto"];
        $descripcion = $data["descripcion"];
        $responsable = $data["responsable"];
        $fecha = $data["fecha"];
        $calificacion = $data["calificacion"];
        $proyecto_id = 0;
        $sql= "INSERT INTO `otras_actividades_proy`(  `nombre_proyecto`, `nombre_actividad`, `responsable`, `fecha_realizacion`, `producto`, `proyect_id` )"
        ."VALUES ('$producto', '$descripcion', '$responsable', '$fecha', '$calificacion', '$proyecto_id')";
        return $this->insertarConsulta($sql);
    } catch (SQLException $e) {
        throw new Exception('Primary key is null');
    }
}
public function updateGestion($data){
    try {
        $producto = $data["producto"];
        $descripcion = $data["descripcion"];
        $responsable = $data["responsable"];
        $fecha = $data["fecha"];
        $calificacion = $data["calificacion"];
        $proyecto_id = $data["id"];
        $sql= "UPDATE `otras_actividades_proy` SET `nombre_proyecto` = '$producto', `nombre_actividad` = '$descripcion', `responsable` = '$responsable' , `fecha_realizacion` = '$fecha', `producto` = '$calificacion' WHERE  `id` = '$proyecto_id' ";
        return $this->updateConsulta($sql);
    } catch (SQLException $e) {
        throw new Exception('Primary key is null');
    }
}
    /**
     * Busca un objeto Otras_actividades en la base de datos.
     * @param otras_actividades objeto con la(s) llave(s) primaria(s) para consultar
     * @return El objeto consultado o null
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
  public function select($otras_actividades){
      $id=$otras_actividades->getId();

      try {
          $sql= "SELECT `id`, `nombre_proyecto`, `nombre_actividad`, `modalidad_participacion`, `responsable`, `fecha_realizacion`, `producto`, `semillero_id`"
          ."FROM `otras_actividades`"
          ."WHERE `id`='$id'";
          $data = $this->ejecutarConsulta($sql);
          for ($i=0; $i < count($data) ; $i++) {
          $otras_actividades->setId($data[$i]['id']);
          $otras_actividades->setNombre_proyecto($data[$i]['nombre_proyecto']);
          $otras_actividades->setNombre_actividad($data[$i]['nombre_actividad']);
          $otras_actividades->setModalidad_participacion($data[$i]['modalidad_participacion']);
          $otras_actividades->setResponsable($data[$i]['responsable']);
          $otras_actividades->setFecha_realizacion($data[$i]['fecha_realizacion']);
          $otras_actividades->setProducto($data[$i]['producto']);
           $semillero = new Semillero();
           $semillero->setId($data[$i]['semillero_id']);
           $otras_actividades->setSemillero_id($semillero);

          }
      return $otras_actividades;      } catch (SQLException $e) {
          throw new Exception('Primary key is null');
      return null;
      }
  }

    /**
     * Modifica un objeto Otras_actividades en la base de datos.
     * @param otras_actividades objeto con la información a modificar
     * @return  Valor de la llave primaria 
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
  public function update($otras_actividades){
      $id=$otras_actividades->getId();
$nombre_proyecto=$otras_actividades->getNombre_proyecto();
$nombre_actividad=$otras_actividades->getNombre_actividad();
$modalidad_participacion=$otras_actividades->getModalidad_participacion();
$responsable=$otras_actividades->getResponsable();
$fecha_realizacion=$otras_actividades->getFecha_realizacion();
$producto=$otras_actividades->getProducto();
$semillero_id=$otras_actividades->getSemillero_id()->getId();

      try {
          $sql= "UPDATE `otras_actividades` SET  `nombre_proyecto`='$nombre_proyecto' ,`nombre_actividad`='$nombre_actividad' ,`modalidad_participacion`='$modalidad_participacion' ,`responsable`='$responsable' ,`fecha_realizacion`='$fecha_realizacion' ,`producto`='$producto' ,`semillero_id`='$semillero_id' WHERE `id`='$id' ";
         return $this->insertarConsulta($sql);
      } catch (SQLException $e) {
          throw new Exception('Primary key is null');
      }
  }
  public function updateO($otras_actividades,$semillero_o,$cumplimiento,$puntos){
      $id=$otras_actividades->getId();
$nombre_proyecto=$otras_actividades->getNombre_proyecto();
$nombre_actividad=$otras_actividades->getNombre_actividad();
$modalidad_participacion=$otras_actividades->getModalidad_participacion();
$responsable=$otras_actividades->getResponsable();
$fecha_realizacion=$otras_actividades->getFecha_realizacion();
$producto=$otras_actividades->getProducto();
$semillero_id=$semillero_o;

      try {
          $sql= "UPDATE `otras_actividades` SET  `nombre_proyecto`='$nombre_proyecto' ,"
                  . "`nombre_actividad`='$nombre_actividad' ,`modalidad_participacion`='$modalidad_participacion' "
                  . ",`responsable`='$responsable' ,`fecha_realizacion`='$fecha_realizacion' "
                  . ",`producto`='$producto' ,`semillero_id`='$semillero_id',`porcentaje`='$cumplimiento',`puntos`='$puntos' WHERE `id`='$id' ";
         return $this->updateConsulta($sql);
      } catch (SQLException $e) {
          throw new Exception('Primary key is null');
      }
  }

    /**
     * Elimina un objeto Otras_actividades en la base de datos.
     * @param otras_actividades objeto con la(s) llave(s) primaria(s) para consultar
     * @return  Valor de la llave primaria eliminada
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
  public function delete($otras_actividades){
      $id=$otras_actividades->getId();

      try {
          $sql ="DELETE FROM `otras_actividades` WHERE `id`='$id'";
          return $this->insertarConsulta($sql);
      } catch (SQLException $e) {
          throw new Exception('Primary key is null');
      }
  }

    /**
     * Busca un objeto Otras_actividades en la base de datos.
     * @return ArrayList<Otras_actividades> Puede contener los objetos consultados o estar vacío
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
  public function listAll(){
      $lista = array();
      try {
          $sql ="SELECT `id`, `nombre_proyecto`, `nombre_actividad`, `modalidad_participacion`, `responsable`, `fecha_realizacion`, `producto`, `semillero_id`"
          ."FROM `otras_actividades`"
          ."WHERE 1";
          $data = $this->ejecutarConsulta($sql);
          for ($i=0; $i < count($data) ; $i++) {
              $otras_actividades= new Otras_actividades();
          $otras_actividades->setId($data[$i]['id']);
          $otras_actividades->setNombre_proyecto($data[$i]['nombre_proyecto']);
          $otras_actividades->setNombre_actividad($data[$i]['nombre_actividad']);
          $otras_actividades->setModalidad_participacion($data[$i]['modalidad_participacion']);
          $otras_actividades->setResponsable($data[$i]['responsable']);
          $otras_actividades->setFecha_realizacion($data[$i]['fecha_realizacion']);
          $otras_actividades->setProducto($data[$i]['producto']);
           $semillero = new Semillero();
           $semillero->setId($data[$i]['semillero_id']);
           $otras_actividades->setSemillero_id($semillero);

          array_push($lista,$otras_actividades);
          }
      return $lista;
      } catch (SQLException $e) {
          throw new Exception('Primary key is null');
      return null;
      }
  }
  public function listAllInformeGestion(){
    try {
        $lista = array();
        $sql ="SELECT `id`, `nombre_proyecto`, `nombre_actividad`, `responsable`, `fecha_realizacion`, `producto`, `proyect_id`"
        ."FROM `otras_actividades_proy`"
        ."WHERE 1";
        $data = $this->ejecutarConsulta($sql);
        for ($i=0; $i < count($data) ; $i++) {
            $aux = array(
                "id" => $data[$i]['id'],
                "producto" => $data[$i]['nombre_proyecto'],
                "descripcion" => $data[$i]['nombre_actividad'],
                "responsable" => $data[$i]['responsable'],
                "fecha" => $data[$i]['fecha_realizacion'],
                "calificacion" => $data[$i]['producto'],
                "proyecto_id" => $data[$i]['proyect_id']
            );
            array_push($lista, $aux);
        }
    return $lista;
    } catch (SQLException $e) {
        throw new Exception('Primary key is null');
    return null;
    }
}
  
  public function listAll_plan($semillero_id,$plan){
      $lista = array();
      try {
          $sql ="SELECT `id`, `nombre_proyecto`, `nombre_actividad`, `modalidad_participacion`, `responsable`, `fecha_realizacion`, `producto`, `semillero_id`"
                  . ", `porcentaje`, `puntos`"
          ."FROM `otras_actividades`"
          ."WHERE `semillero_id` = '$semillero_id' AND `plan_accion_id` = '$plan' ";
//  var_dump($sql);
          $data = $this->ejecutarConsulta($sql);
          for ($i=0; $i < count($data) ; $i++) {
              $otras_actividades= new Otras_actividades();
          $otras_actividades->setId($data[$i]['id']);
          $otras_actividades->setNombre_proyecto($data[$i]['nombre_proyecto']);
          $otras_actividades->setNombre_actividad($data[$i]['nombre_actividad']);
          $otras_actividades->setModalidad_participacion($data[$i]['modalidad_participacion']);
          $otras_actividades->setResponsable($data[$i]['responsable']);
          $otras_actividades->setFecha_realizacion($data[$i]['fecha_realizacion']);
          $otras_actividades->setProducto($data[$i]['producto']);
          $otras_actividades->setCumplimeito($data[$i]['porcentaje']);
          $otras_actividades->setPuntos($data[$i]['puntos']);
           $semillero = new Semillero();
           $semillero->setId($data[$i]['semillero_id']);
           $otras_actividades->setSemillero_id($semillero);

          array_push($lista,$otras_actividades);
          }
      return $lista;
      } catch (SQLException $e) {
          throw new Exception('Primary key is null');
      return null;
      }
  }
  public function listAll_plan_id($id){
      $lista = array();
      try {
          $sql ="SELECT `id`, `nombre_proyecto`, `nombre_actividad`, `modalidad_participacion`, `responsable`, `fecha_realizacion`, `producto`, `porcentaje`, `puntos`"
          ."FROM `otras_actividades`"
          ."WHERE `id` = '$id' ";
  //var_dump($sql);
          $data = $this->ejecutarConsulta($sql);
          for ($i=0; $i < count($data) ; $i++) {
              $otras_actividades= new Otras_actividades();
          $otras_actividades->setId($data[$i]['id']);
          $otras_actividades->setNombre_proyecto($data[$i]['nombre_proyecto']);
          $otras_actividades->setNombre_actividad($data[$i]['nombre_actividad']);
          $otras_actividades->setModalidad_participacion($data[$i]['modalidad_participacion']);
          $otras_actividades->setResponsable($data[$i]['responsable']);
          $otras_actividades->setFecha_realizacion($data[$i]['fecha_realizacion']);
          $otras_actividades->setProducto($data[$i]['producto']);
          $otras_actividades->setCumplimeito($data[$i]['porcentaje']);
          $otras_actividades->setPuntos($data[$i]['puntos']);
           $semillero = new Semillero();
           $semillero->setId($data[$i]['id']);
           $otras_actividades->setSemillero_id($semillero);

          array_push($lista,$otras_actividades);
          }
      return $lista;
      } catch (SQLException $e) {
          throw new Exception('Primary key is null');
      return null;
      }
  }

      public function insertarConsulta($sql){
          $this->cn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sentencia=$this->cn->prepare($sql);
          $sentencia->execute(); 
          $sentencia = null;
          return $this->cn->lastInsertId();
    }
      public function ejecutarConsulta($sql){
          $this->cn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sentencia=$this->cn->prepare($sql);
          $sentencia->execute(); 
          $data = $sentencia->fetchAll();
          $sentencia = null;
          return $data;
    }
     public function updateConsulta($sql)
    {
        try {
            $this->cn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sentencia = $this->cn->prepare($sql);
            $sentencia->execute();
            $rta = 1;
            $sentencia = null;
            return $rta;
        } catch (Exception $e) {
            return 0;
        }
    }
    /**
     * Cierra la conexión actual a la base de datos
     */
  public function close(){
      $cn=null;
  }
  public function getListadoAsistenciaByProducto($id_producto){
    try {
        $lista = array();
        $sql= "SELECT `id`, `id_semillero`, `id_eventos`, `fecha`"
        ."FROM `asistencia_eventos`"
        ."WHERE `id_eventos`='$id_producto'";
        $data = $this->ejecutarConsulta($sql);
        for ($i=0; $i < count($data) ; $i++) {
            $aux = array(
                "id" => $data[$i]['id'],
                "id_semillero" => $data[$i]['id_semillero'],
                "id_evento" => $data[$i]['id_eventos'],
                "fecha" => $data[$i]['fecha']
            );
            array_push($lista, $aux);

        }
    return $lista;    
  } catch (SQLException $e) {
        throw new Exception('Primary key is null');
    return null;
    }
}
public function marcarAsistencia($data){
    try {
        $idSemillero = $data["id_semillero"];
        $idProducto = $data["id_producto"];
        $sql= "INSERT INTO `asistencia_eventos`(  `id_semillero`, `id_eventos`) VALUES ('$idSemillero', '$idProducto')";
        return $this->insertarConsulta($sql);
    } catch (SQLException $e) {
        throw new Exception('Primary key is null');
    }
}
public function quitarAsistencia($data){
    try {
        $idSemillero = $data["id_semillero"];
        $idProducto = $data["id_producto"];
        $sql = "DELETE  FROM `asistencia_eventos` WHERE id_semillero = $idSemillero AND id_eventos = $idProducto";
        return $this->insertarConsulta($sql);
    } catch (SQLException $e) {
      //  throw new Exception('Primary key is null');
    }
}
public function eliminarEvento($id){
    try {
        $sql = "DELETE  FROM `otras_actividades_proy` WHERE id = $id";
        return $this->insertarConsulta($sql);
    } catch (SQLException $e) {
      //  throw new Exception('Primary key is null');
    }
}

}
//That`s all folks!