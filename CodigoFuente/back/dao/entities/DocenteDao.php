<?php
/*
              -------Creado por-------
             \(x.x )/ Anarchy \( x.x)/
              ------------------------
*/

//    ¿Me ayudas con la tesis?  \\
include_once realpath('../dao/interfaz/IDocenteDao.php');
include_once realpath('../dto/Docente.php');
include_once realpath('../dto/Persona.php');
include_once realpath('../dto/Tipo_vinculacion.php');

class DocenteDao implements IDocenteDao
{

    private $cn;

    /**
     * Inicializa una única conexión a la base de datos, que se usará para cada consulta.
     */
    function __construct($conexion)
    {
        $this->cn = $conexion;
    }

    /**
     * Guarda un objeto Docente en la base de datos.
     * @param docente objeto a guardar
     * @return  Valor asignado a la llave primaria
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
    public function insert($docente)
    {
        //      $id=$docente->getId();
        $persona_id = $docente->getPersona_id()
            ->getId();
        $password = $docente->getPassword();
        $tipo_vinculacion_id = $docente->getTipo_vinculacion_id()
            ->getId();
        $ubicacion = $docente->getUbicacion();

        try
        {
            $sql = "INSERT INTO `docente`(  `persona_id`, `password`, `tipo_vinculacion_id`, `ubicacion`)" . "VALUES ('$persona_id','$password','$tipo_vinculacion_id','$ubicacion')";
            return $this->insertarConsulta($sql);
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
        }
    }
    public function insert_colaborador($persona_id, $Tipo_Docente, $tipo_vinculacion_id, $id_semillero,$codigo)
    {

        try
        {
            $sql = "INSERT INTO `docente`(  `persona_id`, `tipo_Docente` , `tipo_vinculacion_id`, `semillero` , `codigo`)" . "VALUES ('$persona_id',  '$Tipo_Docente','$tipo_vinculacion_id', '$id_semillero', '$codigo')";
            return $this->insertarConsulta($sql);
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
        }
    }

    /**
     * Busca un objeto Docente en la base de datos.
     * @param docente objeto con la(s) llave(s) primaria(s) para consultar
     * @return El objeto consultado o null
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
    public function select($docente)
    {
        $id = $docente->getId();

        try
        {
            $sql = "SELECT `id`, `persona_id`, `password`, `tipo_vinculacion_id`, `ubicacion`" . "FROM `docente`" . "WHERE `id`='$id'";
            $data = $this->ejecutarConsulta($sql);
            for ($i = 0;$i < count($data);$i++)
            {
                $docente->setId($data[$i]['id']);
                $persona = new Persona();
                $persona->setId($data[$i]['persona_id']);
                $docente->setPersona_id($persona);
                $docente->setPassword($data[$i]['password']);
                $tipo_vinculacion = new Tipo_vinculacion();
                $tipo_vinculacion->setId($data[$i]['tipo_vinculacion_id']);
                $docente->setTipo_vinculacion_id($tipo_vinculacion);
                $docente->setUbicacion($data[$i]['ubicacion']);

            }
            return $docente;
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
            return null;
        }
    }
    public function existe($codigo)
    {
        try
        {
            $sql = "SELECT `id` FROM `docente` WHERE `codigo`='$codigo'";
            $data = $this->ejecutarConsulta($sql);
            return $data;
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
            return null;
        }
    }
    /**
     * Modifica un objeto Docente en la base de datos.
     * @param docente objeto con la información a modificar
     * @return  Valor de la llave primaria
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
    public function update($docente)
    {
        $id = $docente->getId();
        $persona_id = $docente->getPersona_id()
            ->getId();
        $password = $docente->getPassword();
        $tipo_vinculacion_id = $docente->getTipo_vinculacion_id()
            ->getId();
        $ubicacion = $docente->getUbicacion();

        try
        {
            $sql = "UPDATE `docente` SET`id`='$id' ,`persona_id`='$persona_id' ,`password`='$password' ,`tipo_vinculacion_id`='$tipo_vinculacion_id' ,`ubicacion`='$ubicacion' WHERE `id`='$id' ";
            return $this->insertarConsulta($sql);
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
        }
    }

    public function update2($persona_id, $tipo_vinculacion_id, $ubicacion)
    {

        try
        {
            $sql = "UPDATE `docente` SET  `tipo_vinculacion_id`='$tipo_vinculacion_id' ,`ubicacion`='$ubicacion' WHERE `persona_id`='$persona_id' ";
            return $this->updateConsulta($sql);
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
        }
    }

    /**
     * Elimina un objeto Docente en la base de datos.
     * @param docente objeto con la(s) llave(s) primaria(s) para consultar
     * @return  Valor de la llave primaria eliminada
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
    public function delete($docente)
    {
        $id = $docente->getId();

        try
        {
            $sql = "DELETE FROM `docente` WHERE `id`='$id'";
            return $this->insertarConsulta($sql);
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
        }
    }
    
    public function updateColaborador($codigo, $id_docente)
    {
        try
        {
            $sql = "UPDATE `docente` SET codigo = '$codigo' WHERE `id` = '$id_docente'";
            return $this->insertarConsulta($sql);
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
        }
    }
    public function eliminarColaborador($id)
    {
        try
        {
            $sql = "UPDATE `docente` SET semillero = null WHERE `id`='$id'";
            return $this->insertarConsulta($sql);
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
        }
    }
    /**
     * Busca un objeto Docente en la base de datos.
     * @return ArrayList<Docente> Puede contener los objetos consultados o estar vacío
     * @throws NullPointerException Si los objetos correspondientes a las llaves foraneas son null
     */
    public function listAll()
    {
        $lista = array();
        try
        {
            $sql = "SELECT `id`, `persona_id`, `password`, `tipo_vinculacion_id`, `ubicacion`" . "FROM `docente`" . "WHERE 1";
            $data = $this->ejecutarConsulta($sql);
            for ($i = 0;$i < count($data);$i++)
            {
                $docente = new Docente();
                $docente->setId($data[$i]['id']);
                $persona = new Persona();
                $persona->setId($data[$i]['persona_id']);
                $docente->setPersona_id($persona);
                $docente->setPassword($data[$i]['password']);
                $tipo_vinculacion = new Tipo_vinculacion();
                $tipo_vinculacion->setId($data[$i]['tipo_vinculacion_id']);
                $docente->setTipo_vinculacion_id($tipo_vinculacion);
                $docente->setUbicacion($data[$i]['ubicacion']);

                array_push($lista, $docente);
            }
            return $lista;
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
            return null;
        }
    }
    public function listAll_id()
    {
        $lista = array();
        try
        {
            $sql = "SELECT  persona.nombre, persona.correo,  persona.telefono, tp.descripcion   , s.`semillero_id`
                FROM `persona_has_semillero` s
                INNER JOIN persona
                ON persona.id=s.persona_id
                INNER JOIN docente 
                on persona.id=docente.persona_id
                INNER JOIN tipo_vinculacion as tp
                on docente.tipo_vinculacion_id=tp.id WHERE 1";
            $data = $this->ejecutarConsulta($sql);
            for ($i = 0;$i < count($data);$i++)
            {
                $docente = new Docente();
                $docente->setId($data[$i]['id']);
                $persona = new Persona();
                $persona->setId($data[$i]['persona_id']);
                $docente->setPersona_id($persona);
                $docente->setPassword($data[$i]['password']);
                $tipo_vinculacion = new Tipo_vinculacion();
                $tipo_vinculacion->setId($data[$i]['tipo_vinculacion_id']);
                $docente->setTipo_vinculacion_id($tipo_vinculacion);
                $docente->setUbicacion($data[$i]['ubicacion']);

                array_push($lista, $docente);
            }
            return $lista;
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
            return null;
        }
    }

    public function insertarConsulta($sql)
    {
        $this
            ->cn
            ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sentencia = $this
            ->cn
            ->prepare($sql);
        $sentencia->execute();
        $sentencia = null;
        return $this
            ->cn
            ->lastInsertId();
    }
    public function ejecutarConsulta($sql)
    {
        $this
            ->cn
            ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sentencia = $this
            ->cn
            ->prepare($sql);
        $sentencia->execute();
        $data = $sentencia->fetchAll();
        $sentencia = null;
        return $data;
    }

    public function updateConsulta($sql)
    {
        try
        {
            $this
                ->cn
                ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sentencia = $this
                ->cn
                ->prepare($sql);
            $sentencia->execute();
            $rta = 1;
            $sentencia = null;
            return $rta;
        }
        catch(Exception $e)
        {
            return 0;
        }
    }
    /**
     * Cierra la conexión actual a la base de datos
     */
    public function close()
    {
        $cn = null;
    }
    public function listColaboradoresBySemillero($idSemillero)
    {
        $lista = array();
        try
        {
            $sql = "SELECT d.codigo, p.nombre, p.telefono, p.correo, d.id AS id_docente, p.id AS id_persona FROM docente d INNER JOIN persona p ON d.persona_id = p.id WHERE d.semillero = $idSemillero ";
            $data = $this->ejecutarConsulta($sql);
            for ($i = 0;$i < count($data);$i++)
            {
                $aux = ["id" => $i + 1,  "codigo" => $data[$i]["codigo"],"id_persona" => $data[$i]["id_persona"], "id_docente" => $data[$i]["id_docente"], "nombre" => $data[$i]["nombre"], "correo" => $data[$i]["correo"], "telefono" => $data[$i]["telefono"]];
                array_push($lista, $aux);
            }
            return $lista;
        }
        catch(SQLException $e)
        {
            throw new Exception('Primary key is null');
            return null;
        }
    }
}
//That`s all folks!

