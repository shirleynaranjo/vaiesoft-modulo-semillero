<?php

include_once realpath('../facade/Persona_has_semilleroFacade.php');

$list = Persona_has_semilleroFacade::listAll_Semillero_Tabla();
$rta = "";
foreach ($list as $obj => $Persona_has_semillero) {
    $rta .= "{
        \"id\":\"{$Persona_has_semillero->getsemillero_id()->getid()}\",
	    \"sigla\":\"{$Persona_has_semillero->getsemillero_id()->getSigla()}\",
	    \"nombre\":\"{$Persona_has_semillero->getsemillero_id()->getnombre()}\",
	    \"fecha_creacion\":\"{$Persona_has_semillero->getsemillero_id()->getfecha_creacion()}\",
	    \"departamento\":\"{$Persona_has_semillero->getsemillero_id()->getunidad_academica()}\",
	    \"facultad\":\"{$Persona_has_semillero->getsemillero_id()->getFacultad()}\",
        \"persona_Id\":\"{$Persona_has_semillero->getpersona_id()->getid()}\",
        \"nombreD\":\"{$Persona_has_semillero->getpersona_id()->getnombre()}\",
	    \"correoD\":\"{$Persona_has_semillero->getpersona_id()->getcorreo()}\"
	    },";
}

if ($rta != "") {
    $rta = substr($rta, 0, -1);
    http_response_code(200);
    echo "{\"semilleroPer\":[{$rta}]}";
} else {
    echo "{\"semilleroPer\":[]}";
}
