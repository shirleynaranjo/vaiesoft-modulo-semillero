# Título del proyecto: 
### Aplicativo web para la sistematización del plan de acción e informe de gestión de unidades investigativas UFPS (VAIESOFT).
### Modulo de Semillero de Investigacion.
***
## Índice
1. [Características](#características)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Despliegue](#despliegue)
6. [Demo](#demo)
8. [Autor(es)](#autores)
9. [Institución Académica](#institución-académica)
***
### Características:

- Registrar usuarios
- Recuperar contraseña por medio correo electronico
- Registrar un semillero de investigacion FO-IN-03 
- Generar planes de accion FO-IN-10
- Generar informes de gestion FO-IN-14
- Generear solicitudes de horas de investigacion
- Generar solicitudes de cumplimiento de productos
***
### Tecnologías

  ##### BackEnd
  - PHP
  ##### FrontEnd
  - HTML5
  - CSS3
  - JavaScript
  - Bootstrap
  - jQuery
  - DataTable JS
  
  ***
### IDE

- El proyecto se desarrolla usando [Visual Studio Code](https://code.visualstudio.com/Download)

***
### Despliegue

##### 1. Instalación de LAMP 
- Actualizando Debian Linux, ejecute:
sudo -- sh -c 'apt update && apt upgrade'

- Instale Apache, ejecute:
sudo apt install apache2

- Actualice el Firewall y abra los puertos 80 y 443, ejecute:
sudo ufw allow in "WWW Full"

- Configurar MariaDB:
sudo apt install mariadb-server

- Asegure su servidor MariaDB, escriba:
sudo mysql_secure_installation

- Instalación de PHP 7.3:
sudo apt install php libapache2-mod-php php-gd php-mysql

- Pruebe su configuración de LAMP

##### 2. Instalación del servidor web Apache
Servidor HTTP Apache, también conocido como "Apache". Es un servidor web famoso por promover el crecimiento de la World Wide Web. 
- Por lo tanto, instalaremos Apache en Debian 10, ejecute:
sudo apt install apache2
- Cómo iniciar, detener, reiniciar y obtener el estado del servidor Apache
  - La sintaxis es la siguiente para el comando systemctl:
      - sudo systemctl start apache2.service <-- Start the server 
      - sudo systemctl restart apache2.service <-- Restart the server
      - sudo systemctl stop apache2.service <-- Stop the server
      - sudo systemctl reload apache2.service <-- Reload the server config
      - sudo systemctl status apache2.service <-- Get the server status

##### 3. Actualice el cortafuegos y abra los puertos 80 y 443.
Es importante que abra el puerto TCP 80 (www) y 443 (https) para que funcione LAMP en Debian 10. Escriba los siguientes comandos:
  - sudo ufw allow www
  - sudo ufw allow https
  - sudo ufw status

##### 4. Cómo instalar MariaDB en Debian 10
Ahora tiene un servidor web en funcionamiento. Es hora de instalar MariaDB, que es un reemplazo directo del servidor MySQL.
- Escriba el siguiente comando apt :
    - sudo apt install mariadb-server
- Ejecute el script mysql_secure_installation:
    - sudo mysql_secure_installation

##### 5. importar Base de Datos
mysql -u usuario -p nombre_basededatos < data.sql

##### Para descargar [sql](https://github.com/EdwardMarti/sistema_semillero/blob/master/sqlsemillero.sql)
##### Otra opcion de descarga [sql](https://gitlab.com/shirleynaranjo/vaiesoft-modulo-semillero/-/blob/main/Documentacion/M%C3%B3dulo%20Marco%20Adarme/sqlsemillero.sql)

##### 6. Copiar  la carpeta del proyecto en la carpeta publica
- Ingresar al repositorio y descargar el proyecto [Gitlab](https://gitlab.com/shirleynaranjo/vaiesoft-modulo-semillero/-/tree/main/CodigoFuente)

##### 7. Ingresar al sistema con el usuario y claves por defecto

- Credenciales Iniciales del Sistema 

- usuario: admin@admin.ufps.edu.co

- clave: Ufps2021
***
### Demo
El proyecto se encuentra alojado en el siguiente enlace:
http://nortcoding-demos.tk/semillero/front/view/intro/

ROLES

ADMIN
- Usuario:shirleypaolanv@ufps.edu.co
- clave: password

DIRECTOR SEMILLERO
- Usuario: edward22069@gmail.com
- clave:g9GAHE2


***
### Autor(es)

Proyecto desarrollado por:

- [Edward Alfonso Martinez Hernandez] (<edwardalfonsomh@ufps.edu.co>)(<edward22069@gmail.com>) (<316 5392 716>).
- [Shirley Paola Naranjo Villan] (<shirleypaolanvs@ufps.edu.co>)(<shipana23@gmail.com>) (<314 2429 611>).


***
### Institución Académica   
Proyecto desarrollado para el Curso de Profundización de Software [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]

   [Edward Alfonso Martinez Hernandez]: <https://www.facebook.com/ponchomarti>
   [Shirley Paola Naranjo Villan]: <https://www.linkedin.com/in/shirley-naranjo-7b3b2614a/>
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co
