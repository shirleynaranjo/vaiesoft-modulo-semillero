Context = {};
$(document).ready(function () {
  iniciarTablaC();
  id_Semil = Utilitario.getLocal("id_semillero");
  obtenerDatos();
});

//----------------------------------TABLA----------------------------------

/**
 * @method iniciarTabla
 * Metodo para instanciar la DataTable
 */
function iniciarTablaC() {
  //tabla de alumnos
  $("#listadoCapacitacionesTabla").DataTable({
    responsive: true,
    ordering: true,
    paging: true,
    searching: true,
    info: true,
    lengthChange: false,
    language: {
      emptyTable: "No hay capacitaciones para mostrar...",
      search: "Buscar:",
      info: "_START_ de _MAX_ registros", //_END_ muestra donde acaba _TOTAL_ muestra el total
      infoEmpty: "Ningun registro 0 de 0",
      infoFiltered: "(filtro de _MAX_ registros en total)",
      paginate: {
        first: "Primero",
        previous: "Anterior",
        next: "Siguiente",
        last: "Ultimo",
      },
    },
    columns: [
      {
        data: "producto",
        className: "text-center",
        orderable: true,
      },
      {
        data: "descripcion",
        className: "text-center",
        orderable: true,
      },

      {
        data: "responsable",
        className: "text-center",
        orderable: true,
      },
      {
        data: "fecha",
        className: "text-center",
        orderable: true,
      },
      {
        data: "calificacion",
        className: "text-center",
        orderable: true,
      },
      {
        orderable: false,
        defaultContent: [
          "<div class='text-center'>",
          "<a class='personalizado actualizar' title='Gestionar'><i class='fa fa-edit'></i>&nbsp; &nbsp;  &nbsp;</a>",
          "<a class='personalizado asistencia' title='asistencia'><i class='fa fa-user-o'></i>&nbsp; &nbsp;  &nbsp;</a>",
          "<a class='personalizado eliminar' title='eliminar'><i class='fa fa-trash'></i>&nbsp; &nbsp;  &nbsp;</a>",
          "</div>",
        ].join(""),
      },
    ],
    rowCallback: function (row, data, index) {
      var id_order = data.id;
      $(".asistencia", row).click(function () {
        tomarAsistencia(data);
      });
      $(".actualizar", row).click(function () {
        motrarModalActualizarEvento(data);
      });
      $(".eliminar", row).click(function () {
        DeleteCap(data.id);
      });
    },
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
      {
        extend: "copy",
        className: "btn btn-primary glyphicon glyphicon-duplicate",
      },
      {
        extend: "csv",
        title: "listadoAlumnos",
        className: "btn btn-primary glyphicon glyphicon-save-file",
      },
      {
        extend: "excel",
        title: "listadoAlumnos",
        className: "btn btn-primary glyphicon glyphicon-list-alt",
      },
      {
        extend: "pdf",
        title: "listadoAlumnos",
        className: "btn btn-primary glyphicon glyphicon-file",
      },
      {
        extend: "print",
        className: "btn btn-primary glyphicon glyphicon-print",
        customize: function (win) {
          $(win.document.body).addClass("white-bg");
          $(win.document.body).css("font-size", "10px");
          $(win.document.body)
            .find("table")
            .addClass("compact")
            .css("font-size", "inherit");
        },
      },
    ],
  });
}

//----------------------------------CRUD----------------------------------
/**
 * @method obtenerDatos
 * Método que se encarga de consumir el servicio que devuelve la data para la tabla de alumnos.
 */

function obtenerDatos() {
  Utilitario.agregarMascara();
  fetch("../../back/controller/InformeGestionControllerList.php", {
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Plataform: "web",
    },
    body: JSON.stringify({}),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      listadoTCap(data.data);
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjError(
          "Error",
          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
        );
      }
    })
    .finally(function () {
      Utilitario.quitarMascara();
    });
}

//----------------------------------HELPERS----------------------------------

/**
 * @method listadoEspecialOrdenes
 * Método que se encarga de listar los alumnos a la tabla.
 *
 * @param {Object} orders Arreglo con los datos de las ordenes.
 */

function listadoTCap(data) {
  let tabla = $("#listadoCapacitacionesTabla").DataTable();
  tabla.data().clear();
  tabla.rows.add(data).draw();
}

//<editor-fold defaultstate="collapsed" desc="Modal Capacitaciones">

function mostrarModalCapacitaciones() {
  $("#producto").val("");
  $("#descripcion").val("");
  $("#responsable").val("");
  $("#fecha").val("");
  $("#calificacion").val("");
  $("#myModalCapacitaciones").modal({ show: true });
  $("#btnOrderReg").show();
  $("#btnOrderAct").hide();
}

/**
 * @method ocultarModalOrdenes
 * Método que se encarga de cerrar el modal para registro o actualizacion
 */
function cerrarModalCapacitaciones() {
  $("#myModalCapacitaciones").modal("hide");
}
function registrarProducto() {
  let data = {
    producto: $("#producto").val(),
    descripcion: $("#descripcion").val(),
    responsable: $("#responsable").val(),
    fecha: $("#fecha").val(),
    calificacion: $("#calificacion").val(),
  };

  Utilitario.agregarMascara();
  fetch("../../back/controller/InformeGestionController.php", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Plataform: "web",
    },
    body: JSON.stringify(data),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      cerrarModalCapacitaciones();
      obtenerDatos();
      Mensaje.mostrarMsjExito("Registro Exitoso", data.mensaje);
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjError(
          "Error",
          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
        );
      }
    })
    .finally(function () {
      Utilitario.quitarMascara();
    });
}

function gestionarItem(id_order) {
  cargarInfoTablaLn(id_order);
}

function motrarModalActualizarEvento(data) {
  _Context.actualizarEventoId = data.id;
  $("#producto").val(data.producto);
  $("#descripcion").val(data.descripcion);
  $("#responsable").val(data.responsable);
  $("#fecha").val(data.fecha);
  $("#calificacion").val(data.calificacion);
  $("#btnOrderAct").show();
  $("#btnOrderReg").hide();
  $("#myModalCapacitaciones").modal({ show: true });
}

function dataItemLLenarDatosC(capacitaciones) {
  $("#id_sem").val(capacitaciones[0].id);
  $("#tema_sem").val(capacitaciones[0].tema);
  $("#docente_sem").val(capacitaciones[0].docente);
  $("#fecha_sem").val(capacitaciones[0].fecha);
  $("#cant_capacitados_sem").val(capacitaciones[0].cant_capacitados);

  $("#myModalCapacitaciones").modal({ show: true });
  $("#btnOrderAct").show();
  $("#btnOrderReg").hide();
}

function LimpiarCapacitaciones() {
  $("#id_sem").val("");
  $("#tema_sem").val("");
  $("#docente_sem").val("");
  $("#fecha_sem").val("");
  $("#cant_capacitados_sem").val("");
}
function actualizarEvento() {
  let data = {
    id:parseInt(_Context.actualizarEventoId),
    producto: $("#producto").val(),
    descripcion: $("#descripcion").val(),
    responsable: $("#responsable").val(),
    fecha: $("#fecha").val(),
    calificacion: $("#calificacion").val(),
  };

  Utilitario.agregarMascara();
  fetch("../../back/controller/UpdateInformeGestionController.php", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Plataform: "web",
    },
    body: JSON.stringify(data),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      cerrarModalCapacitaciones();
      obtenerDatos();
      Mensaje.mostrarMsjExito("Registro Exitoso", data.mensaje);
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjError(
          "Error",
          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
        );
      }
    })
    .finally(function () {
      Utilitario.quitarMascara();
    });
}

function DeleteCap(id) {
  Mensaje.mostrarMsjConfirmacion(
    "Eliminar Registro",
    "Este proceso es irreversible , ¿esta seguro que desea este Registro?",
    function () {
      eliminarCapacitacion(id);
    }
  );
}

/**
 * @method AlumnoEliminar
 * Método que se encarga de eliminar el estudiante de todas la bd
 */
function eliminarCapacitacion(id) {
  let data = {
    id: id,
  };
  Utilitario.agregarMascara();
  fetch("../../back/controller/EliminarEventoController.php", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      //                Authorization: JSON.parse(Utilitario.getLocal("user")).token,
      Plataform: "web",
    },

    body: JSON.stringify(data),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      Mensaje.mostrarMsjExito("Borrado Exitoso", data.mensaje);
      id_Semil = Utilitario.getLocal("id_semillero");
      console.log(id_Semil);
      obtenerDatos();
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjError(
          "Error",
          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
        );
      }
    })
    .finally(function () {
      Utilitario.quitarMascara();
    });
}

function getListadoAsistenciaByProducto(id) {
  return new Promise((resolve, reject) => {
    let data = { id_producto: id };
    fetch("../../back/controller/AsistenciaControllerList.php", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Plataform: "web",
      },
      body: JSON.stringify(data),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then(function (data) {
        resolve(data.data);
      })
      .catch(function (promise) {
        console.log("ptomridsf", promise.json);
        if (promise.json) {
          promise.json().then(function (response) {
            console.log("respsada", response);
            let status = promise.status,
              mensaje = response ? response.mensaje : "";
            if (status === 401 && mensaje) {
              Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
                Utilitario.cerrarSesion();
              });
            } else if (mensaje) {
              Mensaje.mostrarMsjError("Error", mensaje);
            }
          });
        }
      });
  });
}

function getListadoSemilleros() {
  return new Promise((resolve, reject) => {
    fetch(
      "../../back/controller/SemilleroController_Perfil_Semillero_ListAll.php",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Plataform: "web",
        },
        body: JSON.stringify({}),
      }
    )
      .then(function (response) {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then(function (data) {
        let aux = {};
        for (let i in data.semilleroPer) {
          aux[data.semilleroPer[i].id] = data.semilleroPer[i];
        }
        resolve(aux);
      });
  });
}
async function tomarAsistencia(data) {
  mostrarModalAsistencia(data);
}

async function mostrarModalAsistencia(data) {
  let asistencia = {};
  let listadoAsistencia = await getListadoSemilleros();
  let aa = await getListadoAsistenciaByProducto(data.id);
  aa.map((item) => {
    asistencia[item.id_semillero] = { id: item.id_semillero, asistio: true };
  });
  let semilleros = listadoAsistencia;
  for (let i in semilleros) {
    let sem = semilleros[i];
    asistencia[sem.id] = !asistencia[sem.id]
      ? { id: sem.id, semillero: sem.nombre, asistio: false }
      : Object.assign(asistencia[sem.id], { semillero: sem.nombre });
  }
  Context.asistencia = asistencia;
  Context.idProducto = data.id;

  let str = "";
  for (let i in asistencia) {
    let a = asistencia[i];
    str += `<tr>
    <td>${i}</td>
    <td>${a.semillero}</td>
    <td class='text-center'>
    
      <input class='form-check-input' id='asistente_${i}' ${
      a.asistio ? "checked" : null
    } type='checkbox' id='flexSwitchCheckDefault'>
    
    </td>
    </tr>`;
  }
  let html = `<div class='table mx-auto'>
  <br />
  <table
    class='table table-striped table-hover table-responsivea'
    id=' '
  >
    <thead>
      <tr class='ufps-tabl'>
        <th style='width: 10px'>ID</th>
        <th>SEMILLERO</th>
        <th>ASISTENCIA</th>
      </tr>
    </thead>
    <tdody>
    ${str}
    </tbody>
  </table>
</div>`;

  document.querySelector("#contenidoModalAsistencia").innerHTML = html;
  $("#modalAsistencia").modal({ show: true });
}

async function guardarAsistencia() {
  let { asistencia, idProducto } = Context;
  let request = [];
  for (let i in asistencia) {
    request.push({
      idSemillero: asistencia[i].id,
      asistio: document.querySelector(`#asistente_${asistencia[i].id}`).checked,
    });
  }
  let asistieron = request.filter((item) => item.asistio);

  let noAsistieron = request;
  await Promise.all(
    noAsistieron.map((item) =>
      quitarAsistencia({ idSemillero: item.idSemillero, idProducto })
    )
  );
  let response = await Promise.all(
    asistieron.map((item) =>
      marcarAsistencia({ idSemillero: item.idSemillero, idProducto })
    )
  );
  $("#modalAsistencia").modal("hide");
  Mensaje.mostrarMsjExito("Finalizado", "Se ha tomado la asistencia");
}

marcarAsistencia = (data) =>
  fetch(`../../back/controller/TomarAsistenciaController.php`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Plataform: "web",
    },
    body: JSON.stringify(data),
  }).then((response) =>
    response.ok
      ? response.json()
      : {
          mensaje: "Peticion Rechazada",
          status: response.status,
        }
  );
quitarAsistencia = (data) =>
  fetch(`../../back/controller/QuitarAsistenciaController.php`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Plataform: "web",
    },
    body: JSON.stringify(data),
  }).then((response) =>
    response.ok
      ? response.json()
      : {
          mensaje: "Peticion Rechazada",
          status: response.status,
        }
  );
