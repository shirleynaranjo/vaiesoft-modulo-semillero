$(document).ready(function () {
  iniciarTabla();
  obtenerDatos();
});

function iniciarTabla() {
  $("#tabla_colaboradores").DataTable({
    responsive: true,
    ordering: true,
    paging: true,
    searching: true,
    info: true,
    lengthChange: false,
    language: {
      emptyTable: "Sin Colaboradores...",
      search: "Buscar:",
      info: "_START_ de _MAX_ registros", //_END_ muestra donde acaba _TOTAL_ muestra el total
      infoEmpty: "Ningun registro 0 de 0",
      infoFiltered: "(filtro de _MAX_ registros en total)",
      paginate: {
        first: "Primero",
        previous: "Anterior",
        next: "Siguiente",
        last: "Ultimo",
      },
    },
    columns: [
      {
        data: "id",
        className: "text-center",
        orderable: true,
      },
      {
        data: "codigo",
        className: "text-center",
        orderable: true,
      },
      {
        data: "nombre",
        className: "text-center",
        orderable: true,
      },
      {
        data: "correo",
        className: "text-center",
        orderable: true,
      },

      {
        data: "telefono",
        className: "text-center",
        orderable: true,
      },
      {
        orderable: false,
        defaultContent: [
          "<div class='text-center'>",
          "<a class='personalizado actualizar' title='Gestionar'><i class='fa fa-edit'></i>&nbsp; &nbsp;  &nbsp;</a>",
          "<a class='personalizado eliminar' title='eliminar'><i class='fa fa-trash'></i></a>",
          "</div>",
        ].join(""),
      },
    ],
    rowCallback: function (row, data, index) {
      $(".actualizar", row).click(function () {
        gestionarItem(data);
      });
      $(".eliminar", row).click(function () {
        eliminarColaborador(data);
      });
    },
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
      {
        extend: "copy",
        className: "btn btn-primary glyphicon glyphicon-duplicate",
      },
      {
        extend: "csv",
        title: "listadoAlumnos",
        className: "btn btn-primary glyphicon glyphicon-save-file",
      },
      {
        extend: "excel",
        title: "listadoAlumnos",
        className: "btn btn-primary glyphicon glyphicon-list-alt",
      },
      {
        extend: "pdf",
        title: "listadoAlumnos",
        className: "btn btn-primary glyphicon glyphicon-file",
      },
      {
        extend: "print",
        className: "btn btn-primary glyphicon glyphicon-print",
        customize: function (win) {
          $(win.document.body).addClass("white-bg");
          $(win.document.body).css("font-size", "10px");
          $(win.document.body)
            .find("table")
            .addClass("compact")
            .css("font-size", "inherit");
        },
      },
    ],
  });
}
function llenarTabla(data) {
  let tabla = $("#tabla_colaboradores").DataTable();
  tabla.data().clear();
  tabla.rows.add(data).draw();
}
function obtenerDatos() {
  let data = { id_semillero: Utilitario.getLocal("id_semillero") };
  fetch("../../back/controller/ColaboradoresControllerListBySemillero.php", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Plataform: "web",
    },
    body: JSON.stringify(data),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      llenarTabla(data.colaboradores);
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjError(
          "Error",
          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
        );
      }
    })
    .finally(function () {
      Utilitario.quitarMascara();
    });
}

async function registrarColaborador() {
  if (!document.querySelector("#codigo").value) {
    Mensaje.mostrarMsjWarning(
      "Informacion",
      "Debes ingresar un codigo de colaborador"
    );
  } else if (!document.querySelector("#nombre").value) {
    Mensaje.mostrarMsjWarning(
      "Informacion",
      "Debes ingresar un nombre de colaborador"
    );
  } else {
    let data = {
      id_semillero: Utilitario.getLocal("id_semillero"),
      codigo: $("#codigo").val(),
      nombre: $("#nombre").val(),
      correo: $("#correo").val(),
      telefono: $("#telefono").val(),
    };
    let existe = await existeColaborador(data.codigo);
    if (existe) {
      Mensaje.mostrarMsjWarning(
        "Informacion",
        "Ya existe un usuario registado con el codigo " + data.codigo
      );
      return null;
    }
    fetch("../../back/controller/DocenteController_Colaborador.php", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Plataform: "web",
      },

      body: JSON.stringify(data),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then(function (data) {
        $("#modalGestionColaboradores").modal("hide");
        Mensaje.mostrarMsjExito("Registro Exitoso", data.mensaje);
        obtenerDatos();
      })
      .catch(function (promise) {
        if (promise.json) {
          promise.json().then(function (response) {
            let status = promise.status,
              mensaje = response ? response.mensaje : "";
            if (status === 401 && mensaje) {
              Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
                Utilitario.cerrarSesion();
              });
            } else if (mensaje) {
              Mensaje.mostrarMsjError("Error", mensaje);
            }
          });
        } else {
          Mensaje.mostrarMsjError(
            "Error",
            "Ocurrió un error inesperado. Intentelo nuevamente por favor."
          );
        }
      })
      .finally(function () {
        Utilitario.quitarMascara();
      });
  }
}

function mostrarModalGestionColaboradores() {
  $("#nombre").val("");
  $("#codigo").val("");
  $("#telefono").val("");
  $("#correo").val("");
  $("#btnOrderAct").hide();
  $("#btnOrderReg").show();
  $("#modalGestionColaboradores").modal({ show: true });
}

function eliminarColaborador(data) {
  Mensaje.mostrarMsjConfirmacion(
    "Eliminar Registros",
    "Este proceso es irreversible , ¿esta seguro que desea eliminar este Registro?",
    function () {
      _eliminarColaborador(data);
    }
  );
}

async function existeColaborador(codigo) {
  try {
    let data = await fetch(
      "../../back/controller/ColaboradorExisteController.php",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Plataform: "web",
        },
        body: JSON.stringify({ codigo }),
      }
    );
    data = await data.json();
    return data.existe.length;
  } catch (error) {}
}
function _eliminarColaborador(data) {
  Utilitario.agregarMascara();
  fetch("../../back/controller/ColaboradorEliminarController.php", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",

      Plataform: "web",
    },

    body: JSON.stringify({ id: data.id_docente }),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      Mensaje.mostrarMsjExito("Borrado Exitoso", data.mensaje);
      obtenerDatos();
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjError(
          "Error",
          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
        );
      }
    })
    .finally(function () {
      Utilitario.quitarMascara();
    });
}

function gestionarItem(data) {
  _Context.colaborador = data;
  $("#nombre").val(data.nombre);
  $("#codigo").val(data.codigo);
  $("#telefono").val(data.telefono);
  $("#correo").val(data.correo);
  $("#btnOrderReg").hide();
  $("#btnOrderAct").show();
  $("#modalGestionColaboradores").modal({ show: true });
}

function actualizarColaborador() {
  let data = {
    id_semillero: Utilitario.getLocal("id_semillero"),
    codigo: $("#codigo").val(),
    nombre: $("#nombre").val(),
    correo: $("#correo").val(),
    telefono: $("#telefono").val(),
    id_docente: _Context.colaborador.id_docente,
    id_persona: _Context.colaborador.id_persona,
  };
  fetch("../../back/controller/DocenteController_UpdateColaborador.php", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Plataform: "web",
    },

    body: JSON.stringify(data),
  })
    .then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw response;
    })
    .then(function (data) {
      $("#modalGestionColaboradores").modal("hide");
      Mensaje.mostrarMsjExito("Registro Exitoso", data.mensaje);
      obtenerDatos();
    })
    .catch(function (promise) {
      if (promise.json) {
        promise.json().then(function (response) {
          let status = promise.status,
            mensaje = response ? response.mensaje : "";
          if (status === 401 && mensaje) {
            Mensaje.mostrarMsjWarning("Advertencia", mensaje, function () {
              Utilitario.cerrarSesion();
            });
          } else if (mensaje) {
            Mensaje.mostrarMsjError("Error", mensaje);
          }
        });
      } else {
        Mensaje.mostrarMsjError(
          "Error",
          "Ocurrió un error inesperado. Intentelo nuevamente por favor."
        );
      }
    })
    .finally(function () {
      Utilitario.quitarMascara();
    });
}
